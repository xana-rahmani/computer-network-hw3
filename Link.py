import time


class Link:
    def __init__(self, u, v, cost):
        self.node1 = u
        self.node2 = v
        self.cost = cost
        self.state = 'up'  # up, down
        self.num_down = 0

    def transfer(self, packet, destination):
        # if packet.type == 'ping':
        #     time.sleep(1)
        #     print('LINK__', 'destination: ', packet.type ,destination, packet.data)
        if self.state == 'down':
            return
        if destination == self.node1.id:
            self.node1.receive(packet)
        elif destination == self.node2.id:
            self.node2.receive(packet)
