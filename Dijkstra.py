import sys


def Dijkstra(source, graph):
    temp = set()
    node = []
    link = []
    cost = []
    for item in graph:
        temp.update(item[0])
        link.append(item[0])
        cost.append(item[1])

    for i in temp:
        node.append(i)

    state = {}  # node: [cost, pre_node]
    N = [source]  # node seen
    num_node = len(node)
    for i in node:
        if {source, i} in link:
            state[i] = [cost[link.index({source, i})], source]
        elif i == source:
            continue
        else:
            state[i] = [sys.maxsize, None]
    while True:
        min = sys.maxsize
        for temp in state.keys():
            if temp in N:
                continue
            if state[temp][0] < min:
                min = state[temp][0]
                chosen = temp
        if min == sys.maxsize:
            remove_key = []
            for key in state.keys():
                if state[key][1] is None:
                    remove_key.append(key)
            for key in remove_key:
                state.pop(key)
            return state
        N.append(chosen)
        for i in node:
            if i in N:
                continue
            if {chosen, i} in link:
                # state = {}  node: [cost, pre_node]
                temp_cost = cost[link.index({chosen, i})] + state[chosen][0]
                if temp_cost < state[i][0]:
                    state[i][0] = temp_cost
                    state[i][1] = chosen
        if len(N) == num_node:
            return state
