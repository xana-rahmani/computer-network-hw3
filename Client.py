from Packet import Packet

test = False


class Client:
    def __init__(self, id):  # id is IP ADDRESS
        self.id = id
        self.Links = {}  # router id, link object
        self.router_id = None

    def transfer(self, packet):
        if packet.type == 'ping' and self.router_id is None:
            print(self.id, 'unreachable')
            return
        link = self.Links[self.router_id]
        link.transfer(packet, self.router_id)

    def ping(self, router_id):
        ping_data = {
            'receive': False,
            'trace': [self.id]
        }
        packet = Packet(type='ping', source=self.id, destination=router_id, data=ping_data)
        self.transfer(packet=packet)

    def receive(self, packet):
        if packet.type == 'ping':
            ping_data = packet.data
            if packet.destination == self.id:
                if not ping_data['receive']:
                    ping_data['receive'] = True
                    ping_data['inf'] = 'ok'
                    ping_data['trace'].append(self.id)
                    packet = Packet(type='ping', source=self.id, destination=packet.source, data=ping_data)
                else:
                    ans = ping_data['trace']
                    print(" ".join(map(str, ans)), end=' ')
                    if ping_data['inf'] == 'ok':
                        print('\n', end='')
                    else:
                        print(ping_data['inf'])
                    return
            else:
                if not ping_data['receive']:
                    ping_data['trace'].append(self.id)
                packet = Packet(type='ping', source=packet.source, destination=packet.destination, data=ping_data)
            self.transfer(packet)
