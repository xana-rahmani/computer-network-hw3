from Router import Router
import Router as RouterFile

from Client import Client
from Packet import Packet
from Link import Link

routers = {}  # int: router object
clients = {}  # string: client object
links = {}  # str(router_1_id) + str(router_2_id): link object
second_time = 0
while True:
    command = input().split()
    if True:
        if not len(command):
            continue
        elif command[0] == 'sec':
            second_time += int(command[1])
            num_ten = second_time // 10
            second_time = second_time % 10
            if not num_ten:
                continue
            for j in range(num_ten):
                for l in links.values():
                    if l.state == 'down' and l.num_down < 2:
                        l.num_down = l.num_down + 1
                        node1 = l.node1
                        node2 = l.node2
                        if node1.id in routers.keys():
                            node1.check_link(l)
                        if node2.id in routers.keys():
                            node2.check_link(l)
                    elif l.state == 'up' and l.num_down >= 2:
                        l.num_down = 0
                        node1 = l.node1.id
                        node2 = l.node2.id
                        if node2 in clients.keys():
                            node1, node2 = node2, node1
                        if node1 in clients.keys():
                            client_id = node1
                            client_node = clients[client_id]

                            router_id = node2
                            router = routers[router_id]

                            link_cost = int(command[3])

                            client_node.Links[router_id] = link
                            client_node.router_id = router_id

                            router.Links[client_id] = link
                            router.Links_alive[link] = 0
                            router.connect_client(dis_id=client_id)
                            router.update_routing_table()
                        else:
                            router_1_id = node1
                            router_2_id = node2
                            link_cost = l.cost

                            router_1 = routers[router_1_id]
                            router_2 = routers[router_2_id]

                            router_1.Links[router_2_id] = l
                            router_1.Links_alive[link] = 0
                            router_2.Links[router_1_id] = l
                            router_2.Links_alive[link] = 0

                            router_1.connect(dis_id=router_2_id)

                            router_1.update_routing_table()
                            router_2.update_routing_table()

        elif command[0] == 'add' and command[1] == 'router':
            router_id = command[2]
            r = Router(router_id)
            routers[router_id] = r
        elif command[0] == 'add' and command[1] == 'client':
            client_id = command[2]
            c = Client(client_id)
            clients[client_id] = c

        elif command[0] == 'connect':
            node1 = command[1]
            node2 = command[2]
            if node2 in clients.keys():
                node1, node2 = node2, node1
            if node1 in clients.keys():
                client_id = node1
                client_node = clients[client_id]

                router_id = node2
                router = routers[router_id]

                link_cost = int(command[3])
                link = Link(u=client_node, v=router, cost=link_cost)
                links[str(router_id) + str(client_id)] = link

                client_node.Links[router_id] = link
                client_node.router_id = router_id

                router.Links[client_id] = link
                router.Links_alive[link] = 0
                router.connect_client(dis_id=client_id)

            else:
                router_1_id = node1
                router_2_id = node2
                link_cost = int(command[3])

                router_1 = routers[router_1_id]
                router_2 = routers[router_2_id]
                link = Link(u=router_1, v=router_2, cost=link_cost)
                links[str(router_1_id) + str(router_2_id)] = link

                router_1.Links[router_2_id] = link
                router_1.Links_alive[link] = 0
                router_2.Links[router_1_id] = link
                router_2.Links_alive[link] = 0

                router_1.connect(dis_id=router_2_id)

        elif command[0] == 'ping':
            client_1_id = command[1]
            client_2_id = command[2]
            client_1 = clients[client_1_id]
            client_1.ping(client_2_id)
        elif command[0] == 'link':
            router_1_id = command[1]
            router_2_id = command[2]
            link_key = router_1_id + router_2_id

            link = None
            if link_key in links.keys():
                link = links[link_key]
            else:
                link_key = router_2_id + router_1_id
                if link_key in links.keys():
                    link = links[link_key]

            if link is None:
                print('this link dont exist')

            if command[3] == 'd':
                link.state = 'down'
            elif command[3] == 'e':
                link.state = 'up'
                link.num_down = 0
                node1 = link.node1.id
                node2 = link.node2.id
                if node2 in clients.keys():
                    node1, node2 = node2, node1
                if node1 in clients.keys():
                    client_id = node1
                    client_node = clients[client_id]

                    router_id = node2
                    router = routers[router_id]

                    link_cost = link.cost

                    client_node.Links[router_id] = link
                    client_node.router_id = router_id

                    router.Links[client_id] = link
                    router.Links_alive[link] = 0
                    router.connect_client(dis_id=client_id)
                    router.update_routing_table()
                else:
                    router_1_id = node1
                    router_2_id = node2
                    link_cost = link.cost

                    router_1 = routers[router_1_id]
                    router_2 = routers[router_2_id]

                    router_1.Links[router_2_id] = link
                    router_1.Links_alive[link] = 0
                    router_2.Links[router_1_id] = link
                    router_2.Links_alive[link] = 0

                    router_1.connect(dis_id=router_2_id)

                    router_1.update_routing_table()
                    router_2.update_routing_table()
        elif command[0] == 'monitor':
            if command[1] == 'e':
                RouterFile.monitor = True
            elif command[1] == 'd':
                RouterFile.monitor = False
        elif command[0] == 'test':
            import Client
            import Router
            Router.test = True
            Client.test = True
            for i in routers.keys():
                print(routers[i].id, routers[i].routers_state)
                # print(routers[i].id, routers[i].free_interface)
                # interface = routers[i].routingTable
                # for j in interface.values():
                #     print(routers[i].interface_to_link[j].cost)
                print("---------------------------------")
