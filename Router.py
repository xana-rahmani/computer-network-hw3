import time, threading, sys
from Packet import Packet
from Dijkstra import Dijkstra
from Link import Link
from queue import Queue

monitor = False
test = False


class Router:
    def __init__(self, id):
        self.id = id

        self.neighbor_routers_id = []

        self.num_interface = 0
        self.free_interface = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.interface_to_link = {}  # num_interface, link object
        self.link_to_interface = {}  # num_interface, link object

        self.LSDB = []  # [[{router_id_1,router_id_2}, cost], ...]
        self.routingTable = {}  # router_id, num_interface

        self.routers_state = {}

        self.Links = {}  # router id, link object
        self.Links_alive = {}  # link_object, num of check (0, 1)

        self.neighbor_clients_id = []

    def transfer(self, packet, connect=False):
        self.update_routing_table()
        if connect:
            link = self.Links[packet.destination]
            link.transfer(packet, packet.destination)
            return
        if packet.type == 'LSA':
            link = self.Links[packet.destination]
            link.transfer(packet, packet.destination)
            return
        if packet.type == 'ping':
            interface = self.routingTable[packet.destination]
            link = self.interface_to_link[interface]
            if link.state == 'down':
                ping_data = packet.data
                ping_data['receive'] = True
                ping_data['inf'] = 'unreachable'
                destination = packet.source
                packet = Packet(type='ping', source=self.id, destination=destination, data=ping_data)
                self.transfer(packet)
                return
        if packet.destination in self.routingTable.keys():
            interface = self.routingTable[packet.destination]
            link = self.interface_to_link[interface]
            if self.id == link.node1.id:
                link_destination = link.node2.id
            else:
                link_destination = link.node1.id
            link.transfer(packet, link_destination)
        elif packet.destination in self.Links.keys():
            link = self.Links[packet.destination]
            if self.id == link.node1.id:
                link_destination = link.node2.id
            else:
                link_destination = link.node1.id
            link.transfer(packet, link_destination)

    def connect(self, dis_id=None, receive_packet=None):
        """ in hello packet: data is array
            in ‫‪DBD   packet‬‬: data is dictionary """

        # check destination address
        if dis_id is None and receive_packet.source is None:
            return
        if dis_id is None:
            dis_id = receive_packet.source

        link = self.Links[dis_id]

        # check state in list
        if dis_id not in self.routers_state.keys():
            self.routers_state[dis_id] = 'down'

        state = self.routers_state[dis_id]
        pacekt = None
        flood_data = []  # array of dictionary:  {'state': 'connect or disconnect', data:[{v,n}, cost]}
        if state == 'down' and self.num_interface == 10:
            return
        if state == 'down' and receive_packet is None:
            pacekt = Packet(type='hello', source=self.id, destination=dis_id, data=self.neighbor_routers_id)
        elif state == 'down' and receive_packet:
            data = receive_packet.data
            if data is not None and self.id in data:
                state = '2-way'
                self.neighbor_routers_id.append(receive_packet.source)
                self.LSDB.append([{self.id, dis_id}, link.cost])  # add link
                flood_data.append({
                            'state': 'connect',
                            'data': [{self.id, dis_id}, link.cost]
                })
                free_interface = self.free_interface.pop()
                self.routingTable[dis_id] = free_interface  # set id router to num interface
                self.interface_to_link[free_interface] = link  # set num interface to link
                self.link_to_interface[link] = free_interface
                self.num_interface += 1
                pacekt = Packet(type='hello', source=self.id, destination=dis_id, data=self.neighbor_routers_id)
            else:
                state = 'init'
                self.neighbor_routers_id.append(receive_packet.source)
                pacekt = Packet(type='hello', source=self.id, destination=dis_id, data=self.neighbor_routers_id)
        elif state == 'init':
            data = receive_packet.data
            if self.id in data:
                state = '2-way'
                self.LSDB.append([{self.id, dis_id}, link.cost])  # add link
                flood_data.append({
                    'state': 'connect',
                    'data': [{self.id, dis_id}, link.cost]
                })
                free_interface = self.free_interface.pop()
                self.routingTable[dis_id] = free_interface  # set id router to num interface
                self.interface_to_link[free_interface] = link  # set num interface to link
                self.link_to_interface[link] = free_interface
                self.num_interface += 1
                # ‫‪DBD‬‬: ‫‪Database‬‬ ‫‪Description‬‬
                pacekt = Packet(type='DBD', source=self.id, destination=dis_id, data=self.LSDB)
        elif state == '2-way' and receive_packet.type == 'DBD':
            data = receive_packet.data
            for item in data:
                if item in self.LSDB:
                    continue
                self.LSDB.append(item)
                flood_data.append({
                    'state': 'connect',
                    'data': item
                })
            state = 'full'
            self.update_routing_table()
            pacekt = Packet(type='DBD', source=self.id, destination=dis_id, data=self.LSDB)

        self.routers_state[dis_id] = state
        if pacekt is not None:
            self.transfer(packet=pacekt, connect=True)
        if len(flood_data):
            self.flood(flood_data=flood_data, recive_information_from=dis_id)

    def connect_client(self, dis_id=None):
        if dis_id is None:  # check destination address
            return
        if self.num_interface >= 10:
            return

        link = self.Links[dis_id]
        flood_data = []  # array of dictionary:  {'state': 'connect or disconnect', data:[{v,n}, cost]}
        self.neighbor_clients_id.append(dis_id)
        self.LSDB.append([{self.id, dis_id}, link.cost])  # add link
        flood_data.append({
            'state': 'connect',
            'data': [{self.id, dis_id}, link.cost]
        })
        free_interface = self.free_interface.pop()
        self.routingTable[dis_id] = free_interface  # set id router to num interface
        self.interface_to_link[free_interface] = link  # set num interface to link
        self.link_to_interface[link] = free_interface
        self.num_interface += 1

        self.update_routing_table()

        if len(flood_data):
            self.flood(flood_data=flood_data, recive_information_from=dis_id)

    def check_link(self, link):
        if self.num_interface == 0:
            return
        if link not in self.Links_alive.keys() or self.Links_alive[link] == 3:
            return
        link_in_LSDB = [{link.node1.id, link.node2.id}, link.cost]
        if link_in_LSDB not in self.LSDB:
            return
        self.Links_alive[link] = self.Links_alive[link] + 1
        if self.Links_alive[link] == 2:
            self.num_interface -= 1
            self.free_interface.append(self.link_to_interface[link])
            self.interface_to_link.pop(self.link_to_interface[link])
            self.link_to_interface.pop(link)
            self.Links_alive.pop(link)
            if link.node1.id != self.id:
                self.Links.pop(link.node1.id)
                self.neighbor_routers_id.remove(link.node1.id)
                if link.node1.id in self.routers_state.keys():
                    self.routers_state.pop(link.node1.id)
                if self.id in link.node1.routers_state.keys():
                    link.node1.routers_state.pop(self.id)
            elif link.node2.id != self.id:
                self.Links.pop(link.node2.id)
                self.neighbor_routers_id.remove(link.node2.id)
                if link.node2.id in self.routers_state.keys():
                    self.routers_state.pop(link.node2.id)
                if self.id in link.node2.routers_state.keys():
                    link.node2.routers_state.pop(self.id)

            self.LSDB.remove(link_in_LSDB)
            self.update_routing_table()

            # flood_data = []  # array of dictionary:  {'state': 'connect or disconnect', data:[{v,n}, cost]}
            flood_data = [{
                'state': 'disconnect',
                'data': [{link.node2.id, link.node1.id}, link.cost]
            }]
            self.flood(flood_data=flood_data, recive_information_from=None)
        return

    def update_routing_table(self):
        routingTable_update = Dijkstra(source=self.id, graph=self.LSDB)
        remove_keys = []
        for i in self.routingTable.keys():
            if i not in routingTable_update.keys():
                remove_keys.append(i)
        for key in remove_keys:
            self.routingTable.pop(key)

        while True:
            for id in routingTable_update.keys():
                if routingTable_update[id][1] not in self.neighbor_routers_id and routingTable_update[id][1] != self.id:
                    routingTable_update[id][1] = routingTable_update[routingTable_update[id][1]][1]
            flag_break = True
            for id in routingTable_update.keys():
                if routingTable_update[id][1] not in self.neighbor_routers_id and routingTable_update[id][1] != self.id:
                    flag_break = False
            if flag_break:
                break
        for id in routingTable_update.keys():
            if routingTable_update[id][1] == self.id:
                link = self.Links[id]
                self.routingTable[id] = self.link_to_interface[link]
                continue
            if id not in self.routingTable.keys():
                self.routingTable[id] = self.routingTable[routingTable_update[id][1]]
            elif self.routingTable[id] != self.routingTable[routingTable_update[id][1]]:
                self.routingTable[id] = self.routingTable[routingTable_update[id][1]]

    def flood(self, flood_data, recive_information_from):
        for neighbor_router_id in self.neighbor_routers_id:
            if recive_information_from is not None and neighbor_router_id == recive_information_from:
                continue
            packet = Packet(type='LSA', source=self.id, destination=neighbor_router_id, data=flood_data)
            self.transfer(packet=packet)

    def receive(self, packet):
        if monitor:
            print(self.id, ': ', packet.type, packet.source, packet.destination, packet.data)
        if packet.type == 'hello' or packet.type == 'DBD':
            self.connect(receive_packet=packet)
        if packet.type == 'LSA':
            change_in_LSDB = False
            data = packet.data
            flood_data = []
            for dic in data:
                if dic['state'] == 'connect':
                    if dic['data'] in self.LSDB:
                        continue
                    self.LSDB.append(dic['data'])
                    change_in_LSDB = True
                    flood_data.append({
                        'state': 'connect',
                        'data': dic['data']
                    })
                if dic['state'] == 'disconnect':
                    if dic['data'] not in self.LSDB:
                        continue
                    self.LSDB.remove(dic['data'])
                    change_in_LSDB = True
                    flood_data.append({
                        'state': 'disconnect',
                        'data': dic['data']
                    })
            if change_in_LSDB:
                self.update_routing_table()
            if flood_data:
                self.flood(flood_data, packet.source)
        if packet.type == 'ping':
            ping_data = packet.data
            if packet.destination == self.id:
                if not ping_data['receive']:
                    ping_data['receive'] = True
                    ping_data['inf'] = 'ok'
                    ping_data['trace'].append(self.id)
                    packet = Packet(type='ping', source=self.id, destination=packet.source, data=ping_data)
                else:
                    ans = ping_data['trace']
                    print(" ".join(map(str, ans)), end=' ')
                if ping_data['inf'] == 'ok':
                    print('\n', end='')
                else:
                    print(ping_data['inf'])
            else:
                if not ping_data['receive']:
                    ping_data['trace'].append(self.id)
                packet = Packet(type='ping', source=packet.source, destination=packet.destination, data=ping_data)
                if packet.destination not in self.routingTable.keys():
                    ping_data['receive'] = True
                    ping_data['inf'] = 'invalid'
                    destination = packet.source
                    packet = Packet(type='ping', source=self.id, destination=destination, data=ping_data)

            self.transfer(packet)
