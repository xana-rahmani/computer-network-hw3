class Packet:
    def __init__(self, type, source, destination, data=None):
        self.type = type
        self.source = source
        self.destination = destination
        self.data = data
